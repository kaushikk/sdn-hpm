package coen233.p3;

import org.junit.Test;

public class TestAccessRule {

	@Test
	public void test() {
		AccessEntry e = new AccessEntry(AccessEntry.ACCESS.allow,AccessEntry.PROTOCOL.tcp, "1.1.1.1","24",80,"2.2.2.2","24",90);
		AccessList list = new AccessList();
		list.getLocalRules().add(e);
		e= new AccessEntry(AccessEntry.ACCESS.deny,AccessEntry.PROTOCOL.tcp, "10.1.1.10","24",180,"3.2.2.21","24",190);
		list.getMandatoryRules().add(e);
		list.setName("acl1");
		
		FirewallManager fw = FirewallManager.getInstance();
		fw.addAccessList(list);
		
		Controller c = new Controller("c1", "9.9.9.10", 8080);
		ControllerCache cCache = ControllerCache.getInstance();
		cCache.addController(c.getName(), c.getIpaddress(), c.getPort());
//		cCache.addController("c1", "9.9.9.10", 8080);
		String output = fw.push("c1", "acl1",false);
		System.out.println(output);
	}

	@Test
	public void testFileImport() {
		CommandProcessor.processCommand("import acl1.txt\n");
	}
}
