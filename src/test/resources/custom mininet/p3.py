#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.topo import Topo



class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        def addHost(net,a,b,c,d):
            name = 'h%d_%d' % (a,d)
            ip = '%d.%d.%d.%d' % (a,b,c,d)
            return net.addHost(name,ip=ip)

        def addcHost(net,a,b,c,d):
            #company host
            name = 'ch%d_%d' % (c,d)
            ip = '%d.%d.%d.%d' % (a,b,c,d)
            return net.addHost(name,ip=ip)

        def addSwitch(net,a):
            name = 's%d' % (a)
            return net.addSwitch(name)    


        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        info('*** Adding hosts\n')
        #add 9.0.0.0 host group
        hosts9 = [ addHost(self,9,0,0,n) for n in range(0,11) ]


        #add 10.0.0.0 host group
        hosts10 = [ addHost(self,10,0,0,n) for n in range(0,11) ]

        #add 11.0.0.0 host group
        hosts11 = [ addHost(self,11,0,0,n) for n in range(0,21) ]

        company = []
        #add 192.0.0.0 company host group
        for i in range(0,6):
            temp = [ addcHost(self,192,168,i,n) for n in range(0,11) ]
            company.append(temp)



        #add code server
        h100 = self.addHost('h100', ip='10.0.0.2')

        #add switches
        info('*** Adding switch\n')
        switches = [ addSwitch(self,n) for n in range(0,8)]

        #add switches for company
        localswitches = [ addSwitch(self,n) for n in range(20,26)]

        # code server switch
        s100 = self.addSwitch('s100')

        #creating links
        info('*** Creating links\n')

        info('*** Creating code server links\n')
        #link code server
        self.addLink(s100,h100)


        info('*** Creating local switches links\n')
        #link company's local switches
        for i in range(0,len(localswitches) - 1):
            self.addLink(localswitches[i],localswitches[i + 1])
            self.addLink(localswitches[i],switches[4])
            self.addLink(localswitches[i],switches[6])


        info('*** Connecting each switch to main topology links\n')
        #Connecting each switch to main topology
        self.addLink(switches[0],switches[1])
        self.addLink(switches[0],switches[2])
        self.addLink(switches[0],switches[4])
        self.addLink(switches[5],switches[1])
        self.addLink(switches[5],switches[2])
        self.addLink(switches[5],switches[3])
        self.addLink(switches[5],switches[0])
        self.addLink(s100,switches[6])
        self.addLink(s100,switches[7])


        info('*** Creating company switches links\n')
        #link main company switches(4567)
        list = (switches[4],switches[5],switches[6],switches[7])
        for i in range(0,len(list)):
            for h in range(i + 1,len(list)):
                self.addLink(list[i],list[h])


        info('*** Creating host 9-10 links\n')
        #link host 9.0 - 11.0
        for h in range(0,len(hosts9)):self.addLink(switches[0],hosts9[h])

        for h in range(0,len(hosts11)):self.addLink(switches[1],hosts11[h])

        for h in range(0,len(hosts11)):self.addLink(switches[2],hosts11[h])

        for h in range(0,len(hosts10)):self.addLink(switches[3],hosts10[h])

        #link company host to switches
        for i in range(0,len(company)):
            for h in range(0,len(company[i])):self.addLink(localswitches[i],company[i][h])


topos = { 'mytopo': ( lambda: MyTopo() ) }