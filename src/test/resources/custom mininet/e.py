#!/usr/bin/python

"""
This example shows how to create an empty Mininet object
(without a topology object) and add nodes to it manually.
"""

from mininet.net import Mininet
from mininet.node import Controller
from mininet.cli import CLI
from mininet.log import setLogLevel, info


def addHost(net,a,b,c,d):
    name = 'h%d%d' % (a,d)
    ipi = "%d.%d.%d.%d" % (a,b,c,d)
    return net.addHost(name,ip=ipi)

def addcHost(net,a,b,c,d):
    #company host
    name = 'ch%d_%d' % (c,d)
    ipi = "%d.%d.%d.%d" % (a,b,c,d)
    return net.addHost(name,ip=ipi)

def addSwitch(net,a):
    name = 's%d' % (a)
    return net.addSwitch(name)    


def emptyNet():

    "Create an empty network and add nodes to it."

    net = Mininet( controller=Controller )

    info( '*** Adding controller\n' )
    net.addController( 'c0' )

    info( '*** Adding hosts\n' )
    h1 = net.addHost( 'h1', ip='10.0.0.1' )
    h2 = net.addHost( 'h2', ip='10.0.0.2' )
    h3 = net.addHost( 'h3', ip='10.0.0.3' )
    h5 = net.addHost( 'h5', ip='10.0.0.5' )
    h6 = net.addHost( 'h6', ip='10.0.0.6' )
    h7 = net.addHost( 'h7', ip='10.0.0.7' )

    info( '*** Adding switch\n' )
    s3 = net.addSwitch( 's3' )
    s2 = net.addSwitch( 's2' )
    s1 = net.addSwitch( 's1' )

    info( '*** Creating links\n' )
    net.addLink( h1, s3 )
    net.addLink( h2, s2 )
    net.addLink( s2, s3 )
    net.addLink( s1, s3 )
    net.addLink( s1, h3)

    net.addLink( s3, h5)
    net.addLink( s1, h6)
    net.addLink( s2, h7)


    info( '*** Starting network\n')
    net.start()

    info( '*** Running CLI\n' )
    CLI( net )

    info( '*** Stopping network' )
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    emptyNet()
