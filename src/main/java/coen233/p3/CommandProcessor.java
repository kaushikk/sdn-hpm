package coen233.p3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import coen233.p3.AccessEntry.ACCESS;
import coen233.p3.AccessEntry.PROTOCOL;

/**
 * Process the commands from UI.
 * @author kaushik
 *
 */
public class CommandProcessor {

	private static final ControllerCache cCache = ControllerCache.getInstance();
	private static final FirewallManager fwMgr = FirewallManager.getInstance();

	/**
	 * Command PARSER entry point
	 * Process the command line
	 * @param line
	 */
	public static void processCommand(String line) {
		//		Options options = getOptions();
		
		try {
			if(line.startsWith("#")|| line.trim().length()==0) {
				return;
			}else if(line.startsWith("controller")) {
				controller(line);
			}else if(line.startsWith("acl")) {
				acl(line);
			}else if(line.startsWith("ace")) {
				ace(line);
			}else if(line.startsWith("push")) {
				push(line);
			}else if(line.startsWith("show")) {
				show(line);
			}else if(line.startsWith("import")) {
				importFile(line);
			}else if(line.startsWith("clear")) {
				clear(line);
			}else if(line.startsWith("provision")) {
				push(line);
			}else if(line.startsWith("preview")) {
				preview(line);
			}else {
				System.out.println("Unknown Command: "+line);
				return;
			}
		} catch (UnsupportedOperationException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Clear controller, acl or all
	 * @param line
	 */
	private static void clear(String line) {
		// Clear controllers, acls, status
		String[] params = line.split("\\s+");
		String cmd = params[1];
		if(cmd.trim().startsWith("co")) {
			if(params.length==2) {
				System.out.println("Clearing all controllers...");
				// remove all controllers
				cCache.removeAllControllers();
			}else {
				String controllerName = params[2];
				if(controllerName==null) {
					System.out.printf("Controller %s not found\n",controllerName);
					return;
				}
				System.out.println("Removed: "+cCache.removeController(controllerName));
				//show the info of a given controller in params[2]
			}
		}else if(cmd.trim().startsWith("acl")) {
			if(params.length==2) {
				//Clear all ACL
				fwMgr.removeAllAcl();
				System.out.println("Cleared all ACLs");
			}else {
				String name = params[2];
				AccessList acl = fwMgr.getAccessList(name);
				if(acl==null) {
					System.out.printf("ACL %s not present \n",name);
					return;
				}
				fwMgr.removeAcl(acl);
				System.out.println("Removed ACL:"+acl.dumpAcl());
				//show the info of a given acl in params[2]
			}
		}else if(cmd.trim().startsWith("all")) {
			System.out.println("---- Controllers----");
			show("clear controller");
			System.out.println();
			System.out.println("-----ACL-----");
			show("clear acl");
		}
	
	}

	/**
	 * Show the details of the system
	 * @param line
	 */
	private static void show(String line) {
		// TODO show controllers, acls, status - shows floodlight controller status
		String[] params = line.split("\\s+");
		String cmd = params[1];
		if(cmd.trim().startsWith("co")) {
			if(params.length==2) {
				System.out.println(cCache.toString());
				//show controllers
			}else {
				String controllerName = params[2];
				if(controllerName==null) {
					System.out.printf("Controller %s not found\n",controllerName);
					return;
				}
				System.out.println(cCache.getController(controllerName));
				//show the info of a given controller in params[2]
			}
		}else if(cmd.trim().startsWith("acl")) {
			if(params.length==2) {
				System.out.println(fwMgr.getAclString());
			}else {
				String name = params[2];
				AccessList acl = fwMgr.getAccessList(name);
				if(acl==null) {
					System.out.printf("ACL %s not present \n",name);
					return;
				}
				System.out.println(acl.dumpAcl());
				//show the info of a given acl in params[2]
			}

		}else if(cmd.trim().startsWith("all")) {
			System.out.println("---- Controllers----");
			show("show controller");
			System.out.println();
			System.out.println("-----ACL-----");
			show("show acl");
		}
	}

	/**
	 * Add a controller
	 * @param line
	 */
	private static void controller(String line) {
		String[] params = line.split("\\s+");
		if(params.length!=4) {
			throw new UnsupportedOperationException("Incorrect number of parameters:\n eg use: controller <controller-name> <ipaddr> <port> ");
		}
		String controllerName = params[1];
		String ipAddress = params[2];
		Long port = Long.parseLong(params[3]);
		Controller c = new Controller(controllerName,ipAddress,port);
		cCache.addController(c);
		System.out.println("Added Controller to Cache");
		System.out.println(c);
	}

	/**
	 * Push to controller
	 * @param line
	 */
	private static void push(String line) {
		System.out.println("INSIDE push()");
		String[] params = line.split("\\s+");
		if(params.length<3) {
			throw new UnsupportedOperationException("Incorrect Parameters for Provision... \n eg use: provision <controller-name> <acl-name");
		}
		String c = params[1];
		String acl = params[2];
		fwMgr.push(c, acl,true);
	}
	
	/**
	 * Preview provisoning 
	 * @param line
	 */
	private static void preview(String line) {
		System.out.println("preview");
		String[] params = line.split("\\s+");
		if(params.length<3) {
			throw new UnsupportedOperationException("Incorrect Parameters for Preview... \n eg use: preview <controller-name> <acl-name");
		}
		String c = params[1];
		String acl = params[2];
		System.out.println(fwMgr.push(c, acl, false));
	}

	/**
	 * Add ACE
	 * @param line
	 */
	private static void ace(String line) {
		// Add access entry
		System.out.println("INSIDE ace()");
		// TODO Auto-generated method stub
		String[] params = line.split("\\s+");
		if(params.length<10) {
			throw new UnsupportedOperationException("Incorrect number of parameters:\n eg use: accessentry <list-name> <permit|deny> <protocol> <source IP address> <source netmask> <source-port> <dest IP address> <dest netmask> <dest-port>  ");
		}
		String listName = params[1];
		String permission = params[2];
		ACCESS a = ACCESS.deny;
		if("allow".equalsIgnoreCase(permission)) {
			a= ACCESS.allow;
		}else if("deny".equalsIgnoreCase(permission)) {
			a=  ACCESS.deny;
		}
		String protocol=params[3];
		PROTOCOL p = PROTOCOL.tcp;
		if("tcp".equalsIgnoreCase(protocol)){
			p=PROTOCOL.tcp;
		}else if("udp".equalsIgnoreCase(protocol)){
			p=PROTOCOL.udp;
		}
		//TODO - Check for optional parameters and parse accordingly
		String source_ip_address = params[4];
		//source mask is mandatory
		String source_netmask=params[5];
		Long source_port = Long.parseLong(params[6]);
		String dstn_ip_address = params[7];
		//destination mask is mandatory
		String dest_netmask=params[8];
		Long dstn_port = Long.parseLong(params[9]);
		AccessEntry ae = new AccessEntry(a, p, source_ip_address,source_netmask,source_port,dstn_ip_address,dest_netmask,dstn_port);
		
		AccessList acl = fwMgr.getAccessList(listName);
		if(acl!=null) {
			if(params.length==11 && params[10].toLowerCase().startsWith("mandat")) {
				acl.addMandatoryRule(ae);
			}else {
				acl.addLocalRule(ae);
			}
		}
	}

	/**
	 * Add Access control list
	 * @param line
	 */
	private static void acl(String line) {
		String[] params = line.split("\\s+");
		if(params.length<2) {
			throw new UnsupportedOperationException("Incorrect number of parameters:\n eg use: accesslist <list-name>  ");
		}
		String listName = params[1];
		AccessList l = new AccessList(listName);
		if(line.contains("parent")) {
			if(params.length!=4) {
				throw new UnsupportedOperationException("Incorrect number of parameters:\n eg use: accesslist <list-name> [parent <acl-name>] ");
			}
			String parentId=params[3];
			if(fwMgr.getAccessList(parentId)!=null) {
				//parent is present
				l.setParentId(parentId);
			}else {
				System.out.printf("Parent ACL %s does not exist\n",parentId);
				return;
			}
		}
		fwMgr.addAccessList(l);
		System.out.println(fwMgr.getAccessList(listName).toString());
	}

	/**
	 * Import contents of a file
	 * @param line
	 */
	private static void importFile(String line) {
		String[] s = line.split(" ");
		BufferedReader reader=null;
		try {
			reader = new BufferedReader(new FileReader(s[1]));
			String l="";
			while((l=reader.readLine())!=null) {
				processCommand(l);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(reader!=null) {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

}
