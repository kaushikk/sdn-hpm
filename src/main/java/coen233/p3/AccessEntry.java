package coen233.p3;

/**
 * Models an Access Control Entry
 * @author kaushik
 *
 */
public class AccessEntry {
	public enum ACCESS {
		allow, deny;
	}
	
	public enum PROTOCOL {
		tcp,udp,icmp;
	}
	private String name;
	private String sourceIp;
	private String sourceNetmask;
	private String desIp;
	private String destNetmask;
	private long sourcePort;
	private long destPort;
	private ACCESS permit;
	private PROTOCOL protocol;
	
	
	public AccessEntry(ACCESS permit, PROTOCOL protocol, String source,
			String sourceNetmask, long sourcePort, String dest,
			String destNetmask, long destPort) {
		super();
		this.permit = permit;
		this.protocol = protocol;
		this.sourceIp = source;
		this.sourceNetmask = sourceNetmask;
		this.sourcePort = sourcePort;
		this.desIp = dest;
		this.destNetmask = destNetmask;
		this.destPort = destPort;
	}
	public AccessEntry(String name) {
		super();
		this.name=name;
	}
	public AccessEntry(String name,ACCESS permit,PROTOCOL protocol, String source, long sourcePort, String dest, long destPort) {
		super();
		this.name=name;
		this.permit = permit;
		this.sourceIp = source;
		this.sourcePort = sourcePort;
		this.desIp = dest;
		this.destPort = destPort;
	}
	public AccessEntry() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the source
	 */
	public String getSourceIp() {
		return sourceIp;
	}
	/**
	 * @param source the source to set
	 */
	public void setSourceIp(String source) {
		this.sourceIp = source;
	}
	/**
	 * @return the sourceNetmask
	 */
	public String getSourceNetmask() {
		return sourceNetmask;
	}
	/**
	 * @param sourceNetmask the sourceNetmask to set
	 */
	public void setSourceNetmask(String sourceNetmask) {
		this.sourceNetmask = sourceNetmask;
	}
	/**
	 * @return the dest_ip_address
	 */
	public String getDestIp() {
		return desIp;
	}
	/**
	 * @param dest the dest_ip_address to set
	 */
	public void setDestIp(String dest) {
		this.desIp = dest;
	}
	/**
	 * @return the destNetMask
	 */
	public String getDestNetmask() {
		return destNetmask;
	}
	/**
	 * @param destNetMask the destNetMask to set
	 */
	public void setDestNetmask(String destNetMask) {
		this.destNetmask = destNetMask;
	}
	/**
	 * @return the sourcePort
	 */
	public long getSourcePort() {
		return sourcePort;
	}
	/**
	 * @param sourcePort the sourcePort to set
	 */
	public void setSourcePort(long sourcePort) {
		this.sourcePort = sourcePort;
	}
	/**
	 * @return the destPort
	 */
	public long getDestPort() {
		return destPort;
	}
	/**
	 * @param destPort the destPort to set
	 */
	public void setDestPort(long destPort) {
		this.destPort = destPort;
	}
	/**
	 * @return the permit
	 */
	public ACCESS getPermit() {
		return permit;
	}
	/**
	 * @param permit the permit to set
	 */
	public void setPermit(ACCESS permit) {
		this.permit = permit;
	}
	/**
	 * @return the protocol
	 */
	public PROTOCOL getProtocol() {
		return protocol;
	}
	/**
	 * @param protocol the protocol to set
	 */
	public void setProtocol(PROTOCOL protocol) {
		this.protocol = protocol;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccessEntry [");
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if (permit != null) {
			builder.append("permit=");
			builder.append(permit);
			builder.append(", ");
		}
		if (protocol != null) {
			builder.append("protocol=");
			builder.append(protocol);
			builder.append(", ");
		}
		if (sourceIp != null) {
			builder.append("sourceIp=");
			builder.append(sourceIp);
			builder.append(", ");
		}
		if (sourceNetmask != null) {
			builder.append("sourceNetmask=");
			builder.append(sourceNetmask);
			builder.append(", ");
		}
		builder.append("sourcePort=");
		builder.append(sourcePort);
		builder.append(", ");
		if (desIp != null) {
			builder.append("desIp=");
			builder.append(desIp);
			builder.append(", ");
		}
		if (destNetmask != null) {
			builder.append("destNetmask=");
			builder.append(destNetmask);
			builder.append(", ");
		}
		builder.append("destPort=");
		builder.append(destPort);
		builder.append("]");
		return builder.toString();
	}
	
	
}
