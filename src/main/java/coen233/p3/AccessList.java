package coen233.p3;

import java.util.ArrayList;
import java.util.List;

/**
 * Models an ACCESS List. An ACCESS List can contain multiple Access Entries
 * The PARENT ID field represents the inheritance from another ACL
 * @author kaushik
 *
 */
public class AccessList {
	private int id;
	private String parentId;
	private String name;
	
	/**
	 * Mandatory Rules are Processed First.
	 *  
	 */
	private List<AccessEntry> mandatoryRules;

	/**
	 * Local Rules are Added Last. 
	 */
	private List<AccessEntry> localRules;
	
	public AccessList() {
		localRules = new ArrayList<AccessEntry>();
		mandatoryRules = new ArrayList<AccessEntry>();
	}
	
	public AccessList(String name) {
		this();
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the mandatoryRules
	 */
	public List<AccessEntry> getMandatoryRules() {
		return mandatoryRules;
	}
	/**
	 * @param mandatoryRules the mandatoryRules to set
	 */
	public void setMandatoryRules(List<AccessEntry> mandatoryRules) {
		this.mandatoryRules = mandatoryRules;
	}
	/**
	 * @return the defaultRules
	 */
//	public List<AccessEntry> getDefaultRules() {
//		return defaultRules;
//	}
//	/**
//	 * @param defaultRules the defaultRules to set
//	 */
//	public void setDefaultRules(List<AccessEntry> defaultRules) {
//		this.defaultRules = defaultRules;
//	}
	/**
	 * @return the localRules
	 */
	public List<AccessEntry> getLocalRules() {
		return localRules;
	}
	/**
	 * @param localRules the localRules to set
	 */
	public void setLocalRules(List<AccessEntry> localRules) {
		this.localRules = localRules;
	}
	
	public void addLocalRule(AccessEntry e){
		this.localRules.add(e);
	}
	
	public void addMandatoryRule(AccessEntry e){
		this.mandatoryRules.add(e);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccessList [");
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if(parentId!=null) {
			builder.append(", parentId=");
			builder.append(parentId);
			builder.append(", ");
		}
		
		builder.append("id=");
		builder.append(id);
		builder.append(", ");
		
		if (mandatoryRules != null) {
			builder.append("mandatoryRules=");
			builder.append(mandatoryRules.size());
			builder.append(", ");
		}
		if (localRules != null) {
			builder.append("localRules=");
			builder.append(localRules.size());
		}
		builder.append("]");
		return builder.toString();
	}

	public String dumpAcl() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccessList [");
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		builder.append("id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append(", ");
		if (mandatoryRules != null) {
			builder.append("mandatoryRules=[").append("\n");
			for(AccessEntry e: mandatoryRules) {
				builder.append("\t").append(e.toString()).append("\n");
			}
			builder.append("]");
			builder.append(", ");
			
		}
		if (localRules != null) {
			builder.append("localRules=").append("\n");
			for(AccessEntry e: localRules) {
				builder.append("\t").append(e.toString()).append("\n");
			}
			builder.append("]");
		}
		builder.append("]");
		return builder.toString();
	}
	
	
}
