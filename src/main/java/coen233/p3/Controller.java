package coen233.p3;

/**
 * Controller Model. Contains Name, IP Address nad Port to connect
 * Supports only TYPE 1 - Floodlight Controller right now.
 * @author kaushik
 *
 */
public class Controller {

	public static final int FLOODLIGHT=1;
	private String name;
	private String ipaddress;
	private long port;
	private int type;

	public Controller(String name, String ip, long port) {
		this.name=name;
		this.ipaddress=ip;
		this.port=port;
		type=FLOODLIGHT;
	}
	/**
	 * @return the ipaddress
	 */
	public String getIpaddress() {
		return ipaddress;
	}
	/**
	 * @param ipaddress the ipaddress to set
	 */
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	/**
	 * @return the port
	 */
	public long getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(long port) {
		this.port = port;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Controller other = (Controller) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
	
	public String getHttpUrl() {
		return "http://"+this.ipaddress+":"+this.port;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Controller [name=" + name + ", ipaddress=" + ipaddress
				+ ", port=" + port + ", type=" + type + "]";
	}
	

	
}
