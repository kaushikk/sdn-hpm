/**
 *
 */
package coen233.p3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

/**
 * Entry Point to application. Launches a CLI and waits for commands
 * 
 * @author kaushik
 *
 */
public class Main {
	
	private static Options options;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line=null;
		try {
			System.out.print(">");
			while((line = reader.readLine())!=null) {
				if(line.equalsIgnoreCase("Exit")) {
					System.exit(0);
				}else {
					CommandProcessor.processCommand(line);
//					System.out.println("You Entered::::"+line);
				}
				System.out.print(">");
			}
		} catch (IOException e) {
			System.err.print("Error in Reading Keyboard Input");
			e.printStackTrace();
		}
	}
	
	
	
	private static Options getOptions() {
		if(options==null) {
			options = new Options();
			Option controller = new Option("controller", true, "Set Controller Host and IP");
			controller.setArgs(2);
			options.addOption(controller);
			
			
		}
		return options;
	}
	
}
