package coen233.p3;

import java.util.HashMap;

/**
 * Model Store for the different Controllers Added to the system.
 * Singleton instance
 * @author kaushik
 *
 */
public class ControllerCache {
	private HashMap<String,Controller> controllers;
	private static ControllerCache instance;
	
	public ControllerCache() {
		controllers=new HashMap<String,Controller>();
	}
	
	public boolean addController(String name, String ip, long port) {
		Controller c = new Controller(name,ip,port);
		return addController(c);
	}
	
	public boolean addController(Controller c) {
		controllers.put(c.getName(),c);
		return true;
	}
	
	public Controller getController(String name) {
		return controllers.get(name);
	}
	
	public static ControllerCache getInstance() {
		if(instance ==null) {
			instance=new ControllerCache();
		}
		return instance;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		for(String c: controllers.keySet()) {
			b.append(controllers.get(c).toString()).append("\n");
		}
		
		return b.toString();
	}

	public Controller removeController(String controllerName) {
		return controllers.remove(controllerName);
	}

	public void removeAllControllers() {
		controllers.clear();
	}
	
	
}
