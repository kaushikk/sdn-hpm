package coen233.p3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

/**
 * THe firewall management engine. Provides interface to support ACL and Provisioning OPerations. 
 * @author kaushik
 *
 */
public class FirewallManager {

	private static final String FW_PROVISION_URL = "/firewall/rules/json";
	private static final String FLOODLIGHT_URL = "/wm";
	private Map<String, AccessList> acls;
	private ObjectMapper mapper = new ObjectMapper();
	private static FirewallManager instance;
	private ControllerCache cCache = ControllerCache.getInstance();
	
	public static FirewallManager getInstance() {
		if(instance==null) {
			instance = new FirewallManager();
		}
		return instance;
	}
	
	private FirewallManager() {
		acls= new HashMap<String, AccessList>();
	}
	
	public boolean addAccessList(AccessList acl) {
		if(acls.containsKey(acl.getName())) {
			removeAcl(acl);
		}
		acls.put(acl.getName(), acl);
		return true;
	}

	public AccessList getAccessList(String name){
		return acls.get(name);
	}
	
	public void removeAcl(AccessList acl) {
		acls.remove(acl);
	}

	public String getAclString() {
		StringBuilder b = new StringBuilder();
		for(String key:acls.keySet()) {
			AccessList list = acls.get(key);
			b.append(list.toString()).append("\n");
		}
		return b.toString();
	}

	public String push(String controllerName, String aclName,boolean provision) {
		//flatten ACL hierarchy
		ArrayList<AccessEntry> list = PolicyManager.flattenRules(acls,acls.get(aclName));
		// send to controller
		String output = push(cCache.getController(controllerName),list, provision);
		return output;
	}

	private String push(Controller controller, ArrayList<AccessEntry> list,boolean provision) {
		Client client = Client.create();
		WebResource res = client.resource(controller.getHttpUrl()+FLOODLIGHT_URL+FW_PROVISION_URL);
		res.type("application/json");
		ArrayNode a = aceToJson(list);
		if(provision) {
			for(int i=0;i<a.size();i++) {
				ObjectNode node = (ObjectNode) a.get(i);
				res.post(node.toString());
				//TODO error handling
				// DUPLICATE Rules
			}
		}
		return a.toString();
	}

	private ArrayNode aceToJson(List<AccessEntry> list) {
		ArrayNode a = mapper.createArrayNode();
		int index = a.size();
		for(int i=0;i<list.size(); i++) {
			AccessEntry e = list.get(i);
			/*
			 * "dl-type": "<ARP or IPv4>", "src-ip": "<A.B.C.D/M>", "dst-ip": "<A.B.C.D/M>",  
		   "nw-proto": "<TCP or UDP or ICMP>", "tp-src": "<short>", "tp-dst": "<short>",  
		   "priority": "<int>", "action": "<ALLOW or DENY>" 
			 */
			ObjectNode n = mapper.createObjectNode();
			//add action
			n.put("action", e.getPermit().toString());
			//add nw proto
			//			String protocol="tcp";
			//			if(e.getProtocol().equalsIgnoreCase("udp")) {
			//				protocol="udp";
			//			}
			n.put("nw-proto",e.getProtocol().toString());
			// add src-ip
			if(e.getSourceNetmask().length()>0)
				n.put("src-ip", e.getSourceIp()+"/"+e.getSourceNetmask());
			else 
				n.put("src-ip", e.getSourceIp());
			//add dst-ip
			if(e.getSourceNetmask().length()>0)
				n.put("dst-ip", e.getDestIp()+"/"+e.getDestNetmask());
			else 
				n.put("dst-ip", e.getDestIp());

			// add tp-src
			if(e.getSourcePort()!= -1L)
				n.put("tp-src", e.getSourcePort());
			//add tp-dest
			if(e.getDestPort()!= -1L)
				n.put("tp-dst", e.getDestPort());
			a.add(n);
		}

		return a;
	}

	public void removeAllAcl() {
		this.acls.clear();
	}
}
