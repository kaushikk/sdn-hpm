package coen233.p3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Policy Hierarcy Processing Engine. 
 * @author kaushik
 *
 */
public class PolicyManager {

	/**
	 * Processes Hierarchical Rules into predefined FLAT Format which is needed to provision to Floodlight Controller. The Flattening is necessary
	 * because Floodlight controller does not understand hierarchy.
	 * @param acls
	 * @param accessList
	 * @return
	 */
	public static ArrayList<AccessEntry> flattenRules(Map<String, AccessList> acls, AccessList accessList) {
		//if parent present, get from teh parent list
		ArrayList<AccessEntry> list = new ArrayList<AccessEntry>();
		processParent(list,acls, accessList);
		// add current mandatory section
		
		//add local rules
		processDefault(list,acls,accessList);
		//add parent local Rules
		
		return list;
	}

	/**
	 * Recursive Processing of Mandatory Rules. Parent Rules 1st. Child Rules Next
	 * @param list
	 * @param acls
	 * @param acl
	 */
	private static void processParent(List<AccessEntry> list, Map<String,AccessList> acls,AccessList acl) {
		AccessList parent = acls.get(acl.getParentId());
		// first add parent's mandatory section // Perform this recursively for each parent
		if(parent!=null) {
			if(acls.get(parent.getParentId())!=null) {
				processParent(list,acls, parent);
			}else {
				list.addAll(parent.getMandatoryRules());
			}
		}else {
			list.addAll(acl.getMandatoryRules());
		}
	}
	
	/**
	 * Recursive Processing of default Rules - Local RUles First, Parent's Local Rules next
	 * @param list
	 * @param acls
	 * @param acl
	 */
	private static void processDefault(List<AccessEntry> list, Map<String,AccessList> acls,AccessList acl) {
		AccessList parent = acls.get(acl.getParentId());
		// first add parent's mandatory section // Perform this recursively for each parent
		list.addAll(acl.getLocalRules());
		if(parent!=null) {
			if(acls.get(parent.getParentId())!=null) {
				processDefault(list,acls, parent);
		
			}
		}
	}
}
