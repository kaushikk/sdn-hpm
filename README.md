# README #

## Hierarchical Policy Management in SDN ##

Propose and implement a multi layered policy management engine in SDN. The enterprise contains various level/hierarchy of network administrators based on different departments. Each level has a set of access, QoS rules. The network rules need to be bound by the global policies that are enforced.The network controller is used to enforce the policy restriction when the decision is to be made on the routing of the traffic.

## IMPORTANT ##

**- DO NOT CHECKIN .project and .classpath files ** 

**- Always do a REBASE from master. DO NOT PULL**

## Version ##

0.1 - Support for Floodlight Controller ACL


###Compilation Instructions ###

**Compiles using 'Gradle'**

1. Compile Project First Time
2. Compiling the project (RUN 'gradlew eclipse')

```
#!shell

gradlew build

```
   This compiles the entire source Code and sets up dependencies

**Configuration**

```
#!shell

gradlew eclipse
```
This  sets up dependencies

*To View All targets* 

```
#!shell

gradlew tasks
```

**Dependencies**

Dependencies are defined in build.gradle

**Deployment instructions**

FROM ECLIPSE:

Run Main class in the project. This will wait for command line input

or 

```
#!shell

java -jar coen283-p3-t6.jar
```
This triggers the main method


### Setting up Eclipse ###

* Summary of set up
**Import Project Into Eclipse**
![Import-project.png](https://bitbucket.org/repo/XAogEE/images/2866151772-Import-project.png)

Step 2
![import-project-2.png](https://bitbucket.org/repo/XAogEE/images/1189855556-import-project-2.png)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact